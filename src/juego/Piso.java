package juego;

import java.awt.Color;

import entorno.Entorno;


public class Piso {
	private int numeroDepiso;
	private double x;
	private double y;
	private double angulo;
	private double alto;
	private Color color;
	
	public Piso(double x,double y, int numeroDepiso) {
		this.numeroDepiso = numeroDepiso;
		this.x = x;
		this.y = y;
		this.alto = 10;
		this.color = color.white;
		this.angulo = 0;
	}
	public void dibujarPiso(Entorno e) {
		e.dibujarRectangulo(x, y, e.ancho(), alto, angulo, color);
	}
}
