package juego;
//nuevo
import java.awt.*;

import java.util.*;

import javax.swing.ImageIcon;

import entorno.Entorno;




public class Velociraptor {
	//varibles de instancia, declaraciones:
		private int x; 				
		private int y; 				
		private int piso = 4; 
		private int raptorMS = 1; 	
		private int aRate; 			
		Image velociraptorDerecha = new ImageIcon("resources/dino.png").getImage(); 			//img hacia der
		Image velociraptorIzquierda = new ImageIcon("resources/dinoMirrored.png").getImage();	//img hacia izq
		Entorno entorno;
		private int direccion; 	
		private int tamano = 50;
		
		
		public Velociraptor(int x, int y, int z, Entorno entorno, int n) {
			this.x = x;
			this.y = y;
			this.raptorMS = 2;
			this.aRate = n;
			this.entorno = entorno;		
		}
		
		public void descenso() {
			if (!estaEnElSuelo()) {
				this.y = this.y + 2;
			}
		}
		
		
		public boolean debeDisparar(int tick) {
			if (tick%this.aRate==0) {
				return true;
			} else return false;
		}
		
		
		public void dibujar() {
			if (this.raptorMS>=0) { 
			entorno.dibujarImagen(this.velociraptorDerecha, this.x, this.y-25, 0);	
			} else if (this.raptorMS<=0) {
				entorno.dibujarImagen(this.velociraptorIzquierda, this.x, this.y-25, 0);
			}
		}
		
		public void movimiento() {
			if ((this.piso==3)&&(this.x>=50)) {
				this.x = this.x + raptorMS;
			} else if (this.piso==2&&this.x<=755) {
				this.x = this.x + raptorMS;
			} else if (this.piso==4&&this.x<=755) {
				this.x = this.x + raptorMS;
			} else if (this.piso==1) {
				this.x = this.x + raptorMS;
			}
			 
		}
		
		public void alternarSentido() {
			if (this.piso == 1) {
				this.raptorMS = -2;
			}else if (this.piso==2) {
				this.raptorMS = 2;
			}else if (this.piso==3) {
				this.raptorMS = -2;
			}
		}
		
		public void actualizaPiso() {
			if (this.y == 545) {
				this.piso = 1;
				this.direccion = 2;
			} else if (this.y == 385) {
				this.piso = 2;
				this.direccion = 1;
			} else if (this.y == 235) {
				this.piso = 3;
				this.direccion = 2;
			} else if (this.y == 85) {
				this.piso = 4;
				this.direccion = 1;
			}
		}
		
		public boolean estaEnElSuelo() {
			boolean pisoUno = (this.y == 545);
			boolean pisoDos = (this.y == 385 && this.x <= 650);
			boolean pisoTres = (this.y == 235 && this.x >= 120);
			boolean pisoCuatro = (this.y == 85 && this.x <= 675);
			
			//if (this.y == 560 || this.y == 400 || this.y == 250 || this.y == 100) 
			if (pisoUno || pisoDos || pisoTres || pisoCuatro) {
				return true;
			} else
				return false;
		}
		
		public boolean seFue() {
			if (this.y >= 535 && this.x <= 1) {
				return true;
			} else return false;
		}
		
		public Rayo tirarLaser() {
			if (this.direccion == 1 ) {
				return new Rayo(this.x + this.tamano, this.y, this.direccion);
			}
			
				return new Rayo(this.x - this.tamano, this.y, this.direccion);
			}

		
		public int getDireccion() {
			return direccion;
		}

		public boolean leTocoElMartillo(Martillo martillo) {
			if(this.x - (85/2) < martillo.getX() + (25/2) && this.x + (85/2)> martillo.getX() - (85/2) && this.y + (85/2) > martillo.getY()
					- (25/2) && this.y - (85/2) < martillo.getY() + 25/2) {
				return true;
			}
			return false;
		}
		
		public int getX() {
			return x;
		}

		public void setX(int x) {
			this.x = x;
		}

		public int getY() {
			return y;
		}

		public void setY(int y) {
			this.y = y;
		}

		public int getPiso() {
			return piso;
		}

		public void setPiso(int piso) {
			this.piso = piso;
		}

		public int getRaptorMS() {
			return raptorMS;
		}

		public void setRaptorMS(int raptorMS) {
			this.raptorMS = raptorMS;
		}

		public int getaRate() {
			return aRate;
		}

		public void setaRate(int aRate) {
			this.aRate = aRate;
		}

		
}
