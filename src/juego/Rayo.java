package juego;
//FAKE
import java.awt.Color;

import entorno.Entorno;
/*clase del velociraptor
 * no esta terminado 
  falta poer terminar */

public class Rayo {
	private double x;
	private double y;
	private int direccion;
	
	public Rayo(double x, double y, int direccion) {
		this.x = x;
		this.y = y;
		this.direccion = direccion;
	}
	
	public void dibujarse(Entorno e) {
		e.dibujarRectangulo(x, y, 25, 10, 0, Color.yellow);
	}
	
	public void mover() {
		if(this.direccion == 1) {
			this.x = this.x + 8;
		}
		if(this.direccion == 2) {
			this.x = this.x - 8;
		}
	}

	public boolean salioDelBorde(Entorno e) {
		if (this.x > e.ancho()) {
			return true;
		}
		if (this.x < e.ancho() - e.ancho()) {
			return true;
		}
		return false;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	
	
	
	

}
