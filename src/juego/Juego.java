package juego;
//nuevo
import java.awt.*;

import javax.swing.*;

import java.util.*;

import entorno.Entorno;

import entorno.Herramientas;

import entorno.InterfaceJuego;


public class Juego extends InterfaceJuego {
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	// variables de instancia de mi juego
	// private Pelota pelota;
	private Martillo martillo;
	private boolean desaparece;
	public final char TECLA_u = 85;

	// generar numero random para los raptors, para el disparo
	public int randomDisparar() {
		Random numAleatorio = new Random();
		int n = numAleatorio.nextInt(250 - 40 + 1) + 25;
		return n;
	}

	// Variables y m�todos propios de cada grupo
	// ...
	// Aca declaro
	private int nivel;
	private int puntos;
	private int kills;
	private int vidas = 4;
	private int contadorTick = 0;
	private Barbariana barbariana;
	private Commodore commodore;
	private Rayo rayos [];
	private Piso pisos[];
	private Velociraptor raptors[];
	Image backGround = new ImageIcon("resources/castleBackGround.png").getImage();
	Image barbarianaDerecha = new ImageIcon("resources/testeo.png").getImage(); // Imagen Barbariana derecha
	Image barbarianaIzquierda = new ImageIcon("resources/testeoMirrored.png").getImage(); // Imagen Barbariana izquierda
	Image barbarianaAgachadaDer = new ImageIcon("resources/barbaAgachadaDerecha.png").getImage();
	Image barbarianaAgachadaIzq = new ImageIcon("resources/barbaAgachadaIzq.png").getImage();
	
	Image finPartidaGanador = new ImageIcon("resources/youWinEdited.jpg").getImage();
	Image finPartidaPerdedor = new ImageIcon("gameOver.png").getImage();

	Juego() {

		// Inicializa el objeto entorno ... aca inicializo
		this.entorno = new Entorno(this, " Castlevania, Barbarianna Viking  - Grupo 4 - v1", 800, 600);

		// Inicializar lo que haga falta para el juego
		// ...
		Piso pisos1 = new Piso((entorno.ancho() / 2), (entorno.alto() - 5), 1); // piso mas bajo
		Piso pisos2 = new Piso((entorno.ancho() / 2 - 150), (entorno.alto() - 165), 2);
		Piso pisos3 = new Piso((entorno.ancho() / 2 + 150), (entorno.alto() - 315), 3);
		Piso pisos4 = new Piso((entorno.ancho() / 2 - 150), (entorno.alto() - 465), 4); // piso mas elevado
		pisos = new Piso[] { pisos1, pisos2, pisos3, pisos4 };
		//-----------------------------------------------------------------------------------------------------//
		
		Velociraptor raptor1 = new Velociraptor(entorno.ancho() - 600, entorno.alto() - 515, 4, entorno,
				randomDisparar());
		Velociraptor raptor2 = new Velociraptor(entorno.ancho() - 300, entorno.alto() - 515, 4, entorno,
				randomDisparar());
		Velociraptor raptor3 = new Velociraptor(entorno.ancho() - 600, entorno.alto() - 365, 3, entorno,
				randomDisparar());
		Velociraptor raptor4 = new Velociraptor(entorno.ancho() - 300, entorno.alto() - 365, 3, entorno,
				randomDisparar());
		raptors = new Velociraptor[] { raptor1, raptor2, raptor3, raptor4 };
		//-----------------------------------------------------------------------------------------------------//
		
		Rayo rayo1 = null;
		Rayo rayo2 = null;
		Rayo rayo3 = null;
		Rayo rayo4 = null;
		rayos = new Rayo[] { rayo1, rayo2, rayo3, rayo4 };
		
		
		
		barbariana = new Barbariana(30, (entorno.alto() - 40), entorno);
		
		commodore = new Commodore(365, 95, entorno);	
		
		martillo = null;
		desaparece = false;

		// Inicia el juego!
		this.entorno.iniciar();
	}

	/**
	 * Durante el juego, el m�todo tick() ser� ejecutado en cada instante y por
	 * lo tanto es el m�todo m�s importante de esta clase. Aqu� se debe
	 * actualizar el estado interno del juego para simular el paso del tiempo (ver
	 * el enunciado del TP para mayor detalle).
	 */

	public void tick() {
						

		
		// main if, mientras no se gane, corre el juego. 
		if (!barbariana.choqueconCompu(commodore)) {
			
		contadorTick += 1;
		entorno.dibujarImagen(backGround, entorno.ancho()-294, entorno.alto()-500, 0);
		
		for (int n = 0; n<4; n++) {
			pisos[n].dibujarPiso(entorno);
		}
		
		if(raptors[0].estaEnElSuelo()){
			rayos[0] = raptors[0].tirarLaser();
		}
		
		
		for (int i = 0; i < 4; i++) {
			if(raptors[i]!=null) {
			
			raptors[i].dibujar();
			raptors[i].movimiento();
			raptors[i].descenso();
			raptors[i].actualizaPiso();
			raptors[i].alternarSentido();
			if (raptors[i].seFue()) {
				raptors[i]=null;
			} 
			}
		}
		
		nivel = (int) barbariana.getPiso();

		barbariana.drawBarba();
		commodore.drawCompu();

		if (entorno.estaPresionada(entorno.TECLA_DERECHA)) {
			barbariana.moverDerecha(barbarianaDerecha);
		}
		if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA)) {
			barbariana.moverIzquierda(barbarianaIzquierda);
		}

		barbariana.dibujarseAgachada(barbarianaAgachadaDer, barbarianaDerecha, barbarianaIzquierda);
		barbariana.jump();
		barbariana.descenso();
		barbariana.subirPiso(entorno);
		barbariana.actualizaPiso();
		barbariana.actualizarAgachada(barbarianaAgachadaDer, barbarianaAgachadaIzq, barbarianaDerecha,
				barbarianaIzquierda);

		// System.out.println(barbariana.choquecon(raptors));

		if (entorno.sePresiono(entorno.TECLA_ESPACIO) && !desaparece) {
			martillo = barbariana.tirarRayo();
			desaparece = true;
		}
		if (this.martillo != null) {
			martillo.dibujarse(entorno);
			martillo.mover();
		}
		if (martillo != null && martillo.salioDelBorde(entorno)) {
			martillo = null;
			desaparece = false;
		}

		for(int v = 0; v < raptors.length; v++) {
        	if(martillo != null && raptors[v] != null) {
        		if(raptors[v].leTocoElMartillo(martillo)) {
        			kills += 1;
        			raptors[v] = null;
            		martillo = null;
            		desaparece = false;
        		}	
        	}
        }
		entorno.cambiarFont("Arial Black", 12, Color.ORANGE);
		entorno.escribirTexto("Puntaje: " + puntos, 650, 20);
		entorno.escribirTexto("Raptors Aniquilados: " + kills, 450, 20);
		entorno.escribirTexto("Nivel: " + nivel, 350, 20);
		
        	for(int rap = 0; rap < raptors.length; rap++) {
            	if(raptors[0] == null && contadorTick>=900) {
            		raptors[0] = new Velociraptor(entorno.ancho() - 600, entorno.alto() - 515, 4, entorno,
            				randomDisparar());
              	}
            	if(raptors[1] == null && contadorTick>=1800) {
            		raptors[1] = new Velociraptor(entorno.ancho() - 300, entorno.alto() - 515, 4, entorno,
            				randomDisparar());
            	}
            	if(raptors[2] == null && contadorTick>=2200) {
            		raptors[2] =  new Velociraptor(entorno.ancho() - 600, entorno.alto() - 365, 3, entorno,
            				randomDisparar());
            	}
            	if(raptors[3] == null && contadorTick>=2900) {
            		raptors[3] = new Velociraptor(entorno.ancho() - 300, entorno.alto() - 365, 3, entorno,
            				randomDisparar());
            	}
            	
            }
        	
		}else if (barbariana.choqueconCompu(commodore)) {
			entorno.dibujarImagen(finPartidaGanador, entorno.ancho()-400, entorno.alto()-300, 0);
			entorno.cambiarFont("Arial Black", 12, Color.WHITE);
			entorno.escribirTexto("Puntaje: " + puntos, 650, 20);
			entorno.escribirTexto("Raptors Aniquilados: " + kills, 450, 20);
			entorno.escribirTexto("Nivel: " + nivel, 350, 20);
		} else if (vidas <= 0) {
			entorno.dibujarImagen(finPartidaPerdedor, entorno.ancho()-400, entorno.alto()-300, 0);
		}
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	} 
}
