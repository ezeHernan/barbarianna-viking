package juego;

import java.awt.*;

import javax.swing.ImageIcon;

import entorno.Entorno;

//TODO

// Gana, perder, imagenes
// Raptors generacion / eliminacion

public class Barbariana {
	private double x;
	private double y;
	private double tamano;
	private double velocidad;
	private Color color;
	private double angulo;
	private int piso = 1;
	private int direccion = 1;
	Image barbariana = new ImageIcon("resources/testeo.png").getImage();
	Entorno entorno;

	public Barbariana(double x, double y, Entorno entorno) {
		this.x = x;
		this.y = y;
		this.tamano = 50;
		this.velocidad = 5;
		this.angulo = 0;
		this.color = color.red;
		this.entorno = entorno;
	}

	public void drawBarba() {
		entorno.dibujarImagen(this.barbariana, this.x, this.y, angulo);
	}

	// metodos
	public void dibujarseAgachada(Image imgAgacha, Image imgDer, Image imgIzq) {
		if (entorno.estaPresionada(entorno.TECLA_ABAJO)) {
			this.barbariana = imgAgacha;
		} 
	}
	
	public boolean seSuperponeIzq() {
		if ((this.piso == 2) && this.y >= 400 && this.y <= 460 && (this.x >= 675 && this.x <= 701)) {
			return true;
		} else if (this.piso == 4 && this.y >= 100 && this.y <= 160 && (this.x >= 675 && this.x <= 701)) {
			return true;
		} else
			return false;
	}

	public boolean seSuperponeDer() {
		if (this.piso == 3 && this.y >= 250 && this.y <= 310 && (this.x <= 118 && this.x >= 112)) {
			return true;
		} else
			return false;
	}

	public void moverDerecha(Image imgDer) {
		if ((this.x < 771) && ((this.x + velocidad) <= 771) && (!seSuperponeDer())) {
			this.barbariana = imgDer;
			this.x = this.x + velocidad;
			this.direccion = 1;
		}
	}
	
	public void actualizarAgachada(Image imgDer, Image imgIzq, Image imgDerPar, Image imgIzqPar) {
		if (entorno.estaPresionada(entorno.TECLA_ABAJO) && (this.direccion == 1)) {
			this.barbariana = imgDer;
		} else if (entorno.estaPresionada(entorno.TECLA_ABAJO) && (this.direccion == 2)) {
			this.barbariana = imgIzq;
		}  else if (!entorno.estaPresionada(entorno.TECLA_ABAJO) && (this.direccion == 1)) {
			this.barbariana = imgDerPar;
		} else if (!entorno.estaPresionada(entorno.TECLA_ABAJO) && (this.direccion == 2)) {
			this.barbariana = imgIzqPar;
		}
	}

	public void moverIzquierda(Image imgIzq) {

		if ((this.x > 30) && ((this.x - velocidad) >= 30) && (!seSuperponeIzq())) {
			this.barbariana = imgIzq;
			this.x = this.x - velocidad;
			this.direccion = 2;
		}
	}

	public boolean subePiso() {
		if ((piso == 1) && (x >= 750)) {
			return true;
		} else if ((piso == 2) && (x <= 30)) {
			return true;
		} else if ((piso == 3) && (x >= 750)) {
			return true;
		} else
			return false;
	}

	public void jump() {
		if ((entorno.sePresiono(entorno.TECLA_ARRIBA)) && (estaEnElSuelo())) {
			this.y -= 58;
		}
	}

	public boolean estaEnElSuelo() {
		boolean pisoUno = (this.y == 560);
		boolean pisoDos = (this.y == 400 && this.x <= 677);
		boolean pisoTres = (this.y == 250 && this.x >= 120);
		boolean pisoCuatro = (this.y == 100 && this.x <= 677);

		// if (this.y == 560 || this.y == 400 || this.y == 250 || this.y == 100)
		if (pisoUno || pisoDos || pisoTres || pisoCuatro) {
			return true;
		} else
			return false;
	}

	public void descenso() {
		if (!estaEnElSuelo()) {
			this.y = this.y + 2;
		}
	}

	public void actualizaPiso() {
		if (this.y == 560) {
			this.piso = 1;
		} else if (this.y == 400) {
			this.piso = 2;
		} else if (this.y == 250) {
			this.piso = 3;
		} else if (this.y == 100) {
			this.piso = 4;
		}
	}

	public boolean puedeSubir() {
		if ((this.piso == 1) && (this.x >= 677)) {
			return true;
		} else if ((this.piso == 2) && (this.x <= 120)) {
			return true;
		} else if ((this.piso == 3) && (this.x >= 684)) {
			return true;
		} else
			return false;
	}

	public void subirPiso(Entorno e) {
		if (e.sePresiono(e.TECLA_CTRL) && (puedeSubir() && (piso == 1))) {

			this.y = 400;
			this.x = 620;
		} else if (e.sePresiono(e.TECLA_CTRL) && (puedeSubir() && (piso == 2))) {

			this.y = 250;
			this.x = 184;
		} else if (e.sePresiono(e.TECLA_CTRL) && (puedeSubir() && (piso == 3))) {

			this.y = 100;
			this.x = 620;
		}
	}

	public boolean choquecon(Velociraptor[] v) {
		return (v[0].getX() - 85 / 2 < this.x + this.tamano / 2) && (v[0].getX() + 85 / 2 > this.x - this.tamano / 2)
				&& (v[0].getY() + 85 / 2 > this.y - this.tamano / 2);
	}

	public boolean choqueconCompu(Commodore c) {
		return (c.getX() + c.getTamano() > x && x + tamano > c.getX() && y + tamano > c.getY()&& y < c.getY() + c.getTamano());
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getPiso() {
		return piso;
	}

	public void setPiso(int piso) {
		this.piso = piso;
	}

	public double getTamano() {
		return tamano;
	}

	public void setTamano(double tamano) {
		this.tamano = tamano;
	}

	public double getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(double velocidad) {
		this.velocidad = velocidad;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public double getAngulo() {
		return angulo;
	}

	public void setAngulo(double angulo) {
		this.angulo = angulo;
	}

	public Martillo tirarRayo() {
		if (this.direccion == 1) {
			return new Martillo(this.x + this.tamano, this.y, this.direccion);
		}
		if (this.direccion == 2) {
			return new Martillo(this.x - this.tamano, this.y, this.direccion);
		}
		return null;
	}

}
