package juego;

import java.awt.*;

import javax.swing.ImageIcon;

import entorno.Entorno;

public class Commodore {
    private double x;
    private double y;
    private double tamano;
    private double angulo = 0;
    Image compu = new ImageIcon("resources/compuEditada.png").getImage();
	Entorno entorno; 

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    
    public double getAngulo() {
        return angulo;
    }

    public void setAngulo(double angulo) {
        this.angulo = angulo;
    }
    

    public double getTamano() {
		return tamano;
	}

	public void setTamano(double tamano) {
		this.tamano = tamano;
	}

	public Commodore(double x,double y, Entorno entorno) {
        this.tamano = 40;
		this.x = x;
        this.y = y;
        this.entorno = entorno;
    }

	public void drawCompu() {
		entorno.dibujarImagen(this.compu, this.x, this.y, angulo);
	}


}